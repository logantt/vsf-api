REST API backend for vue-storefront
===================================
Original doc: https://docs.vuestorefront.io/guide/installation/linux-mac.html

## Installation

**Start a containerized environment**
Starting Elastic, Redis + Vue Storefront API containers:
`docker-compose -f docker-compose.yml -f docker-compose.nodejs.yml up -d`

As a result, all necessary services will be launched:
- Vue Storefront API runtime environment (Node.js with dependencies from `package.json`)
- [ElasticSearch](https://www.elastic.co/products/elasticsearch)
- [Redis](https://redis.io/)
- Kibana (optional)

**Import product catalog**


Product catalog is imported using [elasticdump](https://www.npmjs.com/package/elasticdump), which is installed automatically via project dependency. The default ElasticSearch index name is: `vue_storefront_catalog`

`docker exec -it vuestorefrontapi_app_1 yarn restore`

Then, to update the structures in the database to the latest version (data migrations), do the following:

`docker exec -it vuestorefrontapi_app_1 yarn migrate`

We're using Magento2 example products dataset from this clone: https://github.com/magento/magento2-sample-data (You can find json file in var/magento2-sample-data)

After all these steps you should be able to use the API application!

You can check if everything works just fine by executing the following command:

`curl -i http://localhost:8080/api/catalog/vue_storefront_catalog/product/_search?q=bag&size=50&from=0`
